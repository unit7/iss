package com.unit7.iss.db.dao;

import com.unit7.iss.db.DatabaseFactory;
import com.unit7.iss.model.entity.FileEntity;
import com.unit7.iss.stub.file.FileStubBuilder;
import com.unit7.iss.util.compare.ReflectionComparator;
import junit.framework.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by breezzo on 23.08.15.
 */
public class FileDAOTest {

    private FileDAO fileDAO;
    private FileStubBuilder fileBuilder;

    @Before
    public void setup() {
        fileDAO = new FileDAO();

        fileBuilder = FileStubBuilder.newInstance()
                            .setName("filename")
                            .setContent(new byte[] { 1, 2, 3 });
    }

    @Test
    public void create() {
        final FileEntity file = file();

        fileDAO.create(file);

        final FileEntity fromDb = fileDAO.getById(file.getId());

        Assert.assertTrue(equals(file, fromDb));
    }

    @Test
    public void update() {
        final FileEntity file = file();

        fileDAO.create(file);

        file.setName("filename2");
        file.setContent(new byte[]{ 3, 3 });

        final int updateCount = fileDAO.update(file);
        final FileEntity fromDb = fileDAO.getById(file.getId());

        Assert.assertEquals(1, updateCount);
        Assert.assertTrue(equals(file, fromDb));
    }

    private FileEntity file() {
        return fileBuilder.build();
    }

    private boolean equals(FileEntity a, FileEntity b) {
        return new ReflectionComparator().compare(a, b) == 0;
    }

    @After
    public void tearDown() {
        DatabaseFactory.instance().destroy();
    }
}
