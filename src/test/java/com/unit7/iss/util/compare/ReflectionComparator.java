package com.unit7.iss.util.compare;

import com.google.common.collect.ImmutableSet;
import com.unit7.iss.exception.ApplicationException;
import org.apache.commons.lang.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Set;

/**
 * Created by breezzo on 16.08.15.
 */
public class ReflectionComparator implements Comparator<Object> {
    private static final Logger logger = LoggerFactory.getLogger(ReflectionComparator.class);

    @Override
    public int compare(Object a, Object b) {
        if (a == b) {
            logger.trace("0. {} == {}", a, b);
            return 0;
        }

        if (a == null) {
            logger.trace("-1. a is null");
            return -1;
        }

        if (b == null) {
            logger.trace("1. b is null");
            return 1;
        }

        final Class<?> aClass = a.getClass();
        final Class<?> bClass= b.getClass();

        if (isIterable(aClass) && isIterable(bClass))
            return compareIterables(a, b);

        if (aClass.isArray() && bClass.isArray())
            return compareArrays(a, b);

        // вообще как вариант можно искать общего предка, но для тестов такое должно сойти
        // т.к. в некоторых случаях может приходить прокси
        if (!aClass.isAssignableFrom(bClass) && !bClass.isAssignableFrom(aClass)) {
            logger.trace("-1. different classes. {}, {}, {}, {}", aClass, bClass, a, b);
            return -1;
        }

        if (canCompare(aClass))
            return ((Comparable) a).compareTo(b);

        try {
            return compareFields(a, b);
        } catch (InvocationTargetException | IllegalAccessException  e) {
            throw new ApplicationException(e.getMessage(), e);
        }
    }

    private boolean canCompare(Class c) {
        return Comparable.class.isAssignableFrom(c);
    }

    private boolean isIterable(Class c) {
        return Iterable.class.isAssignableFrom(c);
    }

    private int compareIterables(Object a, Object b) {
        final Iterator<Object> aIt = ((Iterable) a).iterator();
        final Iterator<Object> bIt = ((Iterable) b).iterator();

        while (aIt.hasNext() && bIt.hasNext()) {
            Object first = aIt.next(), second = bIt.next();
            final int compareResult = compare(first, second);
            if (compareResult != 0) {
                logger.trace("{}. {} == {}", compareResult, first, second);
                return compareResult;
            }
        }

        if (bIt.hasNext()) {
            return -1;
        }

        if (aIt.hasNext()) {
            return 1;
        }

        return 0;
    }

    private int compareArrays(Object a, Object b) {
        Object[] arrayA = asArray(a);
        Object[] arrayB = asArray(b);

        if (arrayA.length != arrayB.length)
            return arrayA.length - arrayB.length;

        for (int i = 0; i < arrayA.length; ++i) {
            Object o1 = arrayA[i];
            Object o2 = arrayB[i];

            final int compareResult = compare(o1, o2);

            if (compareResult != 0) {
                logger.trace("{}. o1 = {}, o2 = {}", compareResult, o1, o2);
                return compareResult;
            }
        }

        return 0;
    }

    private Object[] asArray(Object obj) {
        Class<?> c = obj.getClass().getComponentType();

        if (c == boolean.class)
            return ArrayUtils.toObject((boolean[]) obj);

        if (c == byte.class)
            return ArrayUtils.toObject((byte[]) obj);

        if (c == char.class)
            return ArrayUtils.toObject((char[]) obj);

        if (c == short.class)
            return ArrayUtils.toObject((short[]) obj);

        if (c == int.class)
            return ArrayUtils.toObject((int[]) obj);

        if (c == long.class)
            return ArrayUtils.toObject((long[]) obj);

        if (c == float.class)
            return ArrayUtils.toObject((float[]) obj);

        if (c == double.class)
            return ArrayUtils.toObject((double[]) obj);

        return (Object[]) obj;
    }

    private int compareFields(Object a, Object b) throws InvocationTargetException, IllegalAccessException {
        Class<?> aClass = a.getClass();
        final Class<?> bClass = b.getClass();
        final Method[] methods = aClass.getMethods();

        for (Method method : methods) {
            // go through all getters
            if (isSuitableMethod(method)) {
                final int compareResult = compare(method.invoke(a, null), method.invoke(b, null));
                if (compareResult != 0) {
                    logger.trace("{}. method {}. aclass {}, bclass {}, a {}, b {}", compareResult, method, aClass, bClass, a, b);
                    return compareResult;
                }
            }
        }

        return 0;
    }

    private boolean isSuitableMethod(Method method) {
        Set<String> forbiddenMethods = ImmutableSet.of("getClass", "wait", "notify", "notifyAll", "hashCode", "equals");
        return isGetter(method) && !forbiddenMethods.contains(method.getName());
    }

    private boolean isGetter(Method method) {
        return method.getName().startsWith("get") && method.getParameterCount() == 0;
    }
}
