package com.unit7.iss.stub.file;

import com.unit7.iss.base.creation.Builder;
import com.unit7.iss.model.entity.FileEntity;

/**
 * Created by breezzo on 23.08.15.
 */
public class FileStubBuilder implements Builder<FileEntity> {

    public static FileStubBuilder newInstance() {
        return new FileStubBuilder();
    }

    @Override
    public FileEntity build() {
        final FileEntity file = new FileEntity();
        file.setContent(content);
        file.setName(name);

        return file;
    }

    public FileStubBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public FileStubBuilder setContent(byte[] content) {
        this.content = content;
        return this;
    }

    private String name = "filename";
    private byte[] content = new byte[0];
}
