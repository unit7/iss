package com.unit7.iss.model.entity;

import com.google.common.base.MoreObjects;
import com.unit7.iss.model.entity.base.AbstractEntity;
import org.mongodb.morphia.annotations.Entity;

/**
 * Created by breezzo on 23.08.15.
 */
@Entity("file")
public class FileEntity extends AbstractEntity {

    public String getName() {
        return name;
    }

    public byte[] getContent() {
        return content;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("name", name)
                .add("content", content)
                .toString();
    }

    private String name;
    private byte[] content;
}
