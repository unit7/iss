package com.unit7.iss.service.image.impl;

import com.google.common.base.Optional;
import com.unit7.iss.db.dao.ImageDAO;
import com.unit7.iss.model.entity.AlbumImage;
import com.unit7.iss.model.entity.Image;
import com.unit7.iss.service.image.ImageService;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by breezzo on 02.08.15.
 */
@Singleton
public class ImageServiceImpl implements ImageService {
    @Inject
    private ImageDAO imageDAO;

    @Override
    public Optional<Image> getFirstImageByName(String name) {
        return Optional.fromNullable(imageDAO.getFirstImageByName(name));
    }

    @Override
    public void createImage(Image image) {
        imageDAO.create(image);
    }

    @Override
    public AlbumImage generatePreview(Image image, int width, int height) {

        final byte[] content = new ImageResizer(image).resize(width, height);

        final AlbumImage result = new AlbumImage();
        result.setImagePreviewContent(content);
        result.setName(image.getName());
        result.setOriginal(image);

        return result;
    }
}
