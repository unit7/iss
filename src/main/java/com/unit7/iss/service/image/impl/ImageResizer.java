package com.unit7.iss.service.image.impl;

import com.unit7.iss.exception.ApplicationException;
import com.unit7.iss.model.entity.Image;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageInputStream;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * Created by breezzo on 22.08.15.
 */
public class ImageResizer {
    private Image image;

    public ImageResizer(Image image) {
        this.image = image;
    }

    public byte[] resize(int width, int height) {
        try {
            final BufferedImage bufferedImage = createBufferedImage(image.getContent());

            final int newWidth = Math.min(bufferedImage.getWidth(), width);
            final int newHeight = Math.min(bufferedImage.getHeight(), height);

            final BufferedImage resized = resize(bufferedImage, newWidth, newHeight);

            return toByteArray(resized);

        } catch (IOException e) {
            throw new ApplicationException(e.getMessage(), e);
        }
    }

    private BufferedImage createBufferedImage(byte[] content) throws IOException {
        final ByteArrayInputStream imageByteStream = new ByteArrayInputStream(image.getContent());
        final ImageInputStream imageInputStream = ImageIO.createImageInputStream(imageByteStream);

        return ImageIO.read(imageInputStream);
    }

    private BufferedImage resize(BufferedImage original, int width, int height) {
        final BufferedImage resized = new BufferedImage(width, height, original.getType());
        final Graphics2D canvas = resized.createGraphics();

        canvas.drawImage(original, 0, 0, width, height, null);
        canvas.dispose();

        return resized;
    }

    private byte[] toByteArray(BufferedImage image) throws IOException {
        final ByteArrayOutputStream byteOutStream = new ByteArrayOutputStream();

        final boolean writed = ImageIO.write(image, "jpg", byteOutStream); // TODO

        if (writed) {
            return byteOutStream.toByteArray();
        } else {
            throw new ApplicationException("Cannot convert image");
        }
    }
}
