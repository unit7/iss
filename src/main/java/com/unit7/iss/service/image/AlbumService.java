package com.unit7.iss.service.image;

import com.unit7.iss.model.entity.Album;
import com.unit7.iss.model.entity.Image;
import com.unit7.iss.model.entity.User;
import org.bson.types.ObjectId;

import java.util.List;

/**
 * Created by breezzo on 19.08.15.
 */
public interface AlbumService {
    void create(Album album);

    Album get(ObjectId id);

    List<Album> getByUser(User user);

    void addImage(ObjectId albumId, Image image);
}
