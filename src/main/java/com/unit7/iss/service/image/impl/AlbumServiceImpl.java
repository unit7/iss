package com.unit7.iss.service.image.impl;

import javax.inject.Inject;
import javax.inject.Singleton;
import com.unit7.iss.db.dao.AlbumDAO;
import com.unit7.iss.model.entity.Album;
import com.unit7.iss.model.entity.AlbumImage;
import com.unit7.iss.model.entity.Image;
import com.unit7.iss.model.entity.User;
import com.unit7.iss.service.image.AlbumService;
import com.unit7.iss.service.image.ImageService;
import org.bson.types.ObjectId;

import java.util.List;

/**
 * Created by breezzo on 19.08.15.
 */
@Singleton
public class AlbumServiceImpl implements AlbumService {

    private static final int DEFAULT_PREVIEW_WIDTH = 100;
    private static final int DEFAULT_PREVIEW_HEIGHT = 100; // TODO constans

    @Inject
    private AlbumDAO albumDAO;

    @Inject
    private ImageService imageService;

    @Override
    public void create(Album album) {
        albumDAO.create(album);
    }

    @Override
    public Album get(ObjectId id) {
        return albumDAO.getById(id);
    }

    @Override
    public List<Album> getByUser(User user) {
        return albumDAO.getByUser(user);
    }

    @Override
    public void addImage(ObjectId albumId, Image image) {

        final Album album = get(albumId);

        final AlbumImage albumImage = imageService.generatePreview(image, DEFAULT_PREVIEW_WIDTH, DEFAULT_PREVIEW_HEIGHT);
        album.addImage(albumImage);

        albumDAO.update(album);
    }
}