package com.unit7.iss.service.file;

import com.unit7.iss.model.entity.FileEntity;
import org.bson.types.ObjectId;

/**
 * Created by breezzo on 23.08.15.
 */
public interface FileService {
    void create(FileEntity file);

    FileEntity get(ObjectId id);
}
