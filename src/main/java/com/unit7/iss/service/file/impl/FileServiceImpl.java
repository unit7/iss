package com.unit7.iss.service.file.impl;

import com.unit7.iss.db.dao.FileDAO;
import com.unit7.iss.model.entity.FileEntity;
import com.unit7.iss.service.file.FileService;
import org.bson.types.ObjectId;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by breezzo on 23.08.15.
 */
@Singleton
public class FileServiceImpl implements FileService {

    @Inject
    private FileDAO fileDAO;

    @Override
    public void create(FileEntity file) {
        fileDAO.create(file);
    }

    @Override
    public FileEntity get(ObjectId id) {
        return fileDAO.getById(id);
    }
}
