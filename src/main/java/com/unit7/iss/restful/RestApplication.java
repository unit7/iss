package com.unit7.iss.restful;

import com.google.common.collect.Sets;
import com.unit7.iss.model.serializer.ObjectIdSerializer;
import org.bson.types.ObjectId;
import org.codehaus.jackson.Version;
import org.codehaus.jackson.jaxrs.JacksonJaxbJsonProvider;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.codehaus.jackson.map.module.SimpleModule;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.Set;

/**
 * Created by breezzo on 20.08.15.
 */
@ApplicationPath("/iss/rest")
public class RestApplication extends Application {
    @Override
    public Set<Class<?>> getClasses() {
        return Sets.newHashSet(ImageResponder.class,
                               AlbumRestService.class,
                               FileUploadRestService.class);
    }

    @Override
    public Set<Object> getSingletons() {

        final ObjectMapper objectMapper = new ObjectMapper();

        objectMapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        objectMapper.configure(SerializationConfig.Feature.FAIL_ON_EMPTY_BEANS, false);

        final SimpleModule serializeModule = new SimpleModule("SimpleSerializeModule", new Version(1, 0, 0, null));
        serializeModule.addSerializer(ObjectId.class, new ObjectIdSerializer());

        objectMapper.registerModule(serializeModule);

        final JacksonJaxbJsonProvider jaxbProvider = new JacksonJaxbJsonProvider();
        jaxbProvider.setMapper(objectMapper);

        return Sets.newHashSet(jaxbProvider);
    }
}
