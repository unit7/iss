package com.unit7.iss.restful;

import com.google.common.io.ByteStreams;
import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataParam;
import com.unit7.iss.model.entity.FileEntity;
import com.unit7.iss.service.file.FileService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by breezzo on 22.08.15.
 */
@Path("/file")
public class FileUploadRestService {

    private static final Logger logger = LoggerFactory.getLogger(FileUploadRestService.class);

    @Inject
    private FileService fileService;

    @POST
    @Path("/upload/{userId}")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response uploadFile(@PathParam("userId") String userId,
                               @FormDataParam("file") InputStream fileInputStream,
                               @FormDataParam("file") FormDataContentDisposition fileDetail) {

        if (logger.isDebugEnabled()) {
            logger.debug("User [ {} ] try to upload file [ name: {}, size: {}, type: {}, date: {} ]",
                            userId, fileDetail.getFileName(), fileDetail.getSize(), fileDetail.getType(), fileDetail.getCreationDate());
        }

        try {
            final byte[] content = ByteStreams.toByteArray(fileInputStream);

            final FileEntity file = new FileEntity();
            file.setContent(content);
            file.setName(fileDetail.getFileName());

            fileService.create(file);

            return Response.ok(file.getId().toHexString()).build();
        } catch (IOException e) {
            logger.error("Error when read uploaded file", e);
            return Response.serverError().entity("Ошибка при загрузке файла").build();
        }
    }
}
