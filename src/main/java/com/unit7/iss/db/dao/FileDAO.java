package com.unit7.iss.db.dao;

import com.unit7.iss.db.DatabaseFactory;
import com.unit7.iss.db.dao.base.AbstractDAO;
import com.unit7.iss.model.entity.FileEntity;
import org.mongodb.morphia.query.UpdateOperations;

/**
 * Created by breezzo on 23.08.15.
 */
public class FileDAO extends AbstractDAO<FileEntity> {
    private static final String FILE_DATABASE_NAME = DatabaseFactory.ISS_DATABASE_NAME;

    public FileDAO() {
        super(FileEntity.class, FILE_DATABASE_NAME);
    }

    @Override
    public int update(FileEntity entity) {
        if (entity.getId() == null) {
            create(entity);
            return 0;
        }

        final UpdateOperations<FileEntity> updateOps = datastore.createUpdateOperations(FileEntity.class)
                .set("name", entity.getName())
                .set("content", entity.getContent());

        return super.update(entity, updateOps);
    }
}
