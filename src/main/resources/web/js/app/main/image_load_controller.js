/**
 * Created by breezzo on 22.08.15.
 */
angular.module('iss').
    controller('ImageLoadController', ['$scope', 'FileUploader', function($scope, FileUploader) {

        var contextPath = localStorage.getItem('contextPath');

        var uploader = $scope.uploader = new FileUploader({
            url: contextPath + '/rest/image/album/uploadImage',
            method: 'PUT',
            formData: [{
                userId: $scope.ngDialogData.model.userId,
                albumId: $scope.ngDialogData.model.albumId
            }]
        });

        uploader.onCompleteAll = function() {
            $scope.closeThisDialog();
        };

        function lockDialog() {
            $scope.locked = true;
        }

        $scope.loadImage = function() {
            lockDialog();
            $scope.uploader.uploadAll();
        };
    }]);