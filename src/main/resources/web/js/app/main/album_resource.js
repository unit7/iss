angular.module('iss')
    .factory('Albums', ['$resource', function($resource) {
        var contextPath = localStorage.getItem('contextPath');

        return $resource(contextPath + '/rest/image/album/:actionName/:albumId', null, {
            create: {
                actionName: 'create',
                method: 'PUT',
                params: {
                    actionName: 'create'
                }
            },
            get: {
                method: 'GET',
                params: {
                    actionName: 'get',
                    albumId: '579b96649d45b06630fe88ba' //TODO
                }
            },
            getByUser: {
                method: 'GET',
                params: {
                    actionName: 'getByUser',
                    param: '55d6257a9d45b07a9f3e8d3c' // TODO
                }
            }
        });
    }]);