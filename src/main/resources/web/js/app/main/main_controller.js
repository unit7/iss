/**
 * Created by breezzo on 08.08.15.
 */

angular.module('iss').
    directive('albumView', function() {
        var staticRoot = localStorage.getItem('staticRoot');

        return {
            restrict: 'A',
            scope: {},
            templateUrl: staticRoot + '/html/image_view/album_view_template.html',
            link: function(scope, element, attrs) {
                scope.load();
            },
            controller: ['$scope', 'Albums', 'ngDialog', function($scope, Albums, ngDialog) {

                function showMessage(title, message) {
                    ngDialog.open({
                        templateUrl: staticRoot + '/html/main/alert_template.html',
                        className: 'ngdialog-theme-plain',
                        data: {
                            title: title,
                            message: message
                        }
                    });
                }

                $scope.load = function() {

                    Albums.get(null, function(data) {
                        $scope.model = data;
                    }, function(response) {
                        showMessage('Ошибка', response);
                    });
                };

                $scope.reloadCurrentAlbum = function() {
                    Albums.get({ albumId: $scope.model.id },
                        function(data) {
                            data.previous = $scope.model.previous;
                            $scope.model = data;
                        }, function(response) {
                            showMessage('Ошибка', response);
                        });
                };

                $scope.openAlbum = function(album) {
                    var previous = $scope.model;
                    $scope.model = album;
                    $scope.model.previous = previous;
                };

                $scope.back = function() {
                    $scope.model = $scope.model.previous;
                };

                $scope.loadImage = function() {

                    var dialog = ngDialog.open({
                        templateUrl: staticRoot + '/html/image_load/image_load_template.html',
                        controller: 'ImageLoadController',
                        className: 'ngdialog-theme-plain',
                        data: {
                            model: {
                                albumId: $scope.model.id,
                                userId: '55d6257a9d45b07a9f3e8d3c' //TODO id
                            }
                        }
                    });

                    dialog.closePromise.then(function(value) {
                        $scope.reloadCurrentAlbum();
                    });
                };
            }]
        };
    }).
    directive('imagePreview', function() {
        var staticRoot = localStorage.getItem('staticRoot');

        return {
            restrict: 'A',
            scope: {
                image: '&'
            },
            templateUrl: staticRoot + '/html/image_view/image_view_template.html',
            link: function(scope, element, attrs) {

                var image = scope.image();
                var imageIndex = attrs.imageIndex;

                var img = element.find('img');

                img[0].src = 'data:image/png;base64,' + image.imagePreviewContent;

                scope.model = {
                    imageIndex: imageIndex,
                    image: image
                }
            }
        };
    });